import java.io.Serializable;

public class Libro implements Comparable<Libro>, Serializable{
    
    private static final long serialVersionUID = 1L;
    private String titolo;
    private String autore;
    private String genere;
    private int pagine;
    private double prezzo;

    public Libro(){

    }

    public Libro(String titolo, String autore,String genere,  int pagine, double prezzo){
        this.titolo = titolo;
        this.autore = autore;
        this.genere = genere;
        this.pagine = pagine;
        this.prezzo = prezzo;
    }

    public Libro(Libro l){
        titolo = l.getTitolo();
        autore = l.getAutore();
        genere = l.getGenere();
        pagine = l.getPagine();
        prezzo = l.getPrezzo();
    }

    public void setTitolo(String titolo){
        this.titolo = titolo;
    }

    public String getTitolo() {
        return this.titolo;
    }


    public String getAutore() {
        return this.autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public String getGenere(){
        return this.genere;
    }

    public void setGenere(String genere){
        this.genere = genere;
    }

    public int getPagine() {
        return this.pagine;
    }

    public void setPagine(int pagine) {
        this.pagine = pagine;
    }

    public double getPrezzo() {
        return this.prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    @Override
    public String toString(){
        String s = "";
        s += "Titolo : " + getTitolo() + System.lineSeparator();
        s += "Autore : " + getAutore() + System.lineSeparator();
        s += "Pagine : " + getPagine() + System.lineSeparator();
        s += "Prezzo : " + getPrezzo() + System.lineSeparator();
        return s;
    }

    public Libro clone(){
        return new Libro(getTitolo(), getAutore(), getGenere(), getPagine(), getPrezzo());
    }

    @Override
    public int compareTo(Libro l){
        return Double.compare(getPrezzo(), l.getPrezzo());
    }

    @Override
    public boolean equals(Object o){
        if( this == o)
            return true;
        else if( o instanceof Libro){
            Libro l = (Libro) o;
            return (getTitolo() == l.getTitolo() && getAutore() == l.getAutore() && getGenere() == l.getGenere() && getPagine() == l.getPagine() && getPrezzo() == l.getPrezzo());
        }
        return false;
    }
}

