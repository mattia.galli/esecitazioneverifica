import java.math.BigDecimal;

public class Prezzo {

   /*  BigDecimal prezzo;
    String genere;


    public Prezzo() {
    }

    public Prezzo(BigDecimal prezzo, String genere) {
        this.prezzo = prezzo;
        this.genere = genere;
    }

    public BigDecimal getPrezzo() {
        return this.prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    public String getGenere() {
        return this.genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public Prezzo prezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public Prezzo genere(String genere) {
        this.genere = genere;
        return this;
    }

    public Prezzo clone(){
        return new Prezzo(getPrezzo(), getGenere());
    }
    @Override
    public String toString() {
        String s = "";
        s += "Genere : " + genere + System.lineSeparator();
        s += "Prezzo : " + prezzo + System.lineSeparator();
        return s;
    } */
    
    BigDecimal prezzo;
    String genere;

    public Prezzo() {
    }

    public Prezzo(BigDecimal prezzo, String genere) {
        this.prezzo = prezzo;
        this.genere = genere;
    }

    public BigDecimal getPrezzo() {
        return this.prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    public String getGenere() {
        return this.genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public Prezzo prezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public Prezzo genere(String genere) {
        this.genere = genere;
        return this;
    }

    public Prezzo clone(){
        return new Prezzo(prezzo, genere);
    }

    @Override
    public String toString() {
        return "{" +
            " prezzo='" + getPrezzo() + "'" +
            ", genere='" + getGenere() + "'" +
            "}";
    }

}
