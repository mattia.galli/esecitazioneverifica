import java.util.*;
import java.io.*;
public class Scaffale {
    
    ArrayList<Libro> lista = new ArrayList<Libro>();

    public void sort(){
        Collections.sort(lista);
    }

    public void add(Libro l){
        lista.add(l.clone());
    }

    public void remove(Libro l){
        lista.remove(l);
    }

    public String toString(){
        String s = "";
        for(Libro l : lista){
            s += l.toString() + System.lineSeparator();
        }
        return s;
    }

    /* public void loadData(String nomeFile) throws IOException{
        Scanner scanner  = new Scanner(new File(nomeFile)).useDelimiter(System.lineSeparator());
        try{
        while(scanner.hasNext()){
            String[] valori = scanner.nextLine().split(";");
            lista.add(new Libro(valori[0], valori[1], valori[2], Integer.valueOf(valori[3]), Double.valueOf(valori[4])));
        }
        } catch(Exception e){

        } finally{
            scanner.close();
        } 
    }

    public void save() throws IOException{
        FileOutputStream file = new FileOutputStream(new File("lista.bin"));
        ObjectOutputStream object = new ObjectOutputStream(file);
        object.writeObject(lista);
        file.close();
        object.close();
    }

    @SuppressWarnings("unchecked")
    public void load() throws IOException, ClassNotFoundException{
        FileInputStream file = new FileInputStream(new File("lista.bin"));
        ObjectInputStream object = new ObjectInputStream(file);
        lista = (ArrayList<Libro>) object.readObject();
    } */

	public int getSize(){
        return lista.size();
    }

    public Libro getLibro(int i){
        return lista.get(i);
    }

    public void loadData(String nomeFile) throws IOException{
        Scanner scanner = new Scanner(new File(nomeFile)).useDelimiter(System.lineSeparator());
        try{
            while(scanner.hasNext()){
                String[] valori = scanner.nextLine().split(";");
                lista.add(new Libro(valori[0], valori[1], valori[2], Integer.valueOf(valori[3]), Double.valueOf(valori[4])));
            }
        }   
    }
}
